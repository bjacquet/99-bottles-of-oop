class Bottles
  def song
    verses(99, 0)
  end

  def verses(a, b)
    (b..a).to_a.reverse.map do |bottles_count|
      verse(bottles_count)
    end.join("\n")

  end

  def verse(bottles_count)
    if bottles_count == 0
      return "No more bottles of beer on the wall, no more bottles of beer.\n"\
        "Go to the store and buy some more, 99 bottles of beer on the wall.\n"
    end

    bottles_count_to_s = bottles_count > 1 ? "bottles" : "bottle"
    remaining_bottles = bottles_count - 1
    remaining_bottles_to_s = remaining_bottles > 1 ? "bottles" : "bottle"

    first_line = "#{bottles_count} #{bottles_count_to_s} of beer on the wall, #{bottles_count} #{bottles_count_to_s} of beer.\n"

    second_line =
      if remaining_bottles == 0
        "Take it down and pass it around, no more bottles of beer on the wall.\n"
      else
        "Take one down and pass it around, #{remaining_bottles} #{remaining_bottles_to_s} of beer on the wall.\n"
      end

    first_line + second_line
  end
end
